# 保卫小麦 整蛊玩法使用教程

### 玩法介绍
主播在一块农田里播种小麦种子并且施肥长成麦子，当农田里区域内全部都是成长的小麦的时候进入十秒倒计时，当倒计时结束时则视为成功~直播间内的观众可以送出礼物达到破坏的目或者帮主播修复~
### 一些游戏中的指令
+ /wrd lw <span style="margin-left: 100px;">:point_right: 打开礼物界面</span>
+ /wrd dev<span style="margin-left: 100px;">:point_right: 打开开发者模式，可以破坏方块，再次输入关闭</span>
+ /wrd topPos<span style="margin-left: 100px;">:point_right: 设置出生点，就是摁 F键 回城的那个</span>
+ /wrd kill<span style="margin-left: 100px;">:point_right: 清除所有实体（包括生物）</span>
+ /wrd kj 扩建格数<span style="margin-left: 100px;">:point_right: 手动扩建指定格数</span>
+ /wrd cc 拆除格数<span style="margin-left: 100px;">:point_right: 手动拆除指定格数</span>
+ /wrd 进度 进度数<span style="margin-left: 100px;">:point_right: 手动调整今日进度数</span>
+ /wrd 总进度 进度数<span style="margin-left: 100px;">:point_right: 手动调整今日总进度数</span>
+ /wrd 移动速度 0.5<span style="margin-left: 100px;">:point_right: 玩家移动的速度范围是0.1 ~ 1.0</span>
+ /wrd 飞行速度 0.5<span style="margin-left: 100px;">:point_right: 玩家移动的速度范围是0.1 ~ 1.0</span>



### 拥有的整蛊效果
+ 生物摧毁小麦
+ 场地扩建/拆除
+ 生物种植小麦
+ 生成蜜蜂/猪猪捣乱
+ 螺旋飞天（使主播飞天指定秒数，脚下会不断生成TNT炸弹）
+  :white_check_mark: 更多功能等待您的意见和想法！
### 更新日志
#### 4.0.2 &nbsp;&nbsp;&nbsp;&nbsp;发布日期 :tw-1f680: 2024-06-03 01:06  &nbsp;&nbsp;&nbsp;&nbsp;[点击获取插件](https://gitee.com/wurendao/DyBhzjGame/releases/tag/4.0.2)
+ 提高玩家种植速度，浇水和种植可以同时进行
+ 新增修改移动速度指令 /wrd 移动速度 0.5    玩家移动的速度范围是0.1 ~ 1.0
+ 新增修改飞行速度指令 /wrd 飞行速度 0.5    玩家移动的速度范围是0.1 ~ 1.0
+ 新增摁 F键 可以回到基地